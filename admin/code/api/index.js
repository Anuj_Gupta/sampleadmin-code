// process.env.DB_DRIVER
require("dotenv").config();
const express = require("express"); //get express framework and initialized
const bodyParser = require("body-parser");
const trimBody = require("connect-trim-body");
const cors = require('cors');
const morgan = require('morgan');
// const compression = require('compression');


const app = express();
const port = process.env.PORT || process.env.SERVER_PORT || 5000;

// compress all requests
// app.use(compression());

// set body parser to read body param
app
    .use(cors())
    .use(bodyParser.urlencoded({ limit: "80mb", extended: false }))
    //Middelware logger 
    .use(morgan('dev'))
    .use(bodyParser.json({ limit: "80mb" }))
    //trim body param values
    .use(trimBody())
    // serve static content from public folder
    .use(express.static("public"))

    // base path show some information about api
    .get("/", (req, res) => res.send("FLOOR INDEX API V1"));

// listen on app
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
}).setTimeout(800000);

// register router
require("./core")(app);
//Register database and elastic search
require("./lib/database")(app);


module.exports = app;
