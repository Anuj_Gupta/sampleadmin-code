/**
 * Please use below convensions to set error
 * 1001 - 1499 generic error
 */
const message = {
    401: {
        "message": "unauthorised access",
        "httpCode": 401
    },
    403: {
        "message": "You don't have permission to perform this action",
        "httpCode": 403
    },
    1001: {
        "message": "Something went wrong on server",
        "httpCode": 500
    },
    1002: {
        "message": "Param error",
        "httpCode": 400
    },
    

    //Property Status Code
    2001:{
        "message": "Property Added Successfully",
        "httpCode": 200
    },
    2002:{
        "message": "Property Updated Successfully",
        "httpCode": 200
    },
    2003:{
        "message": "Property Deleted Successfully",
        "httpCode": 200
    },
    2004:{
        "message": "Property Not Found",
        "httpCode": 200
    },
    2005:{
        "message": "Property list",
        "httpCode": 200
    },
    2006:{
        "message": "Property Published",
        "httpCode": 200
    },
    2007:{
        "message": "Property UnPublished",
        "httpCode": 200
    }
}

module.exports = message