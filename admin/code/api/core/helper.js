let bluebird = require('bluebird');
// const gm = require("gm");
// Promise.promisifyAll(gm.prototype);
const aws = require('aws-sdk');
// const s3 = bluebird.promisifyAll(new aws.S3());
const { BUCKET, AWS_BUCKET_KEY, AWS_BUCKET_SECRET } = process.env;
const fs = require('fs');
const request = require('request');
const winstonLogger = require('./winston');

let s3 = new aws.S3({
  signatureVersion: 'v4',
  accessKeyId: AWS_BUCKET_KEY,
  secretAccessKey: AWS_BUCKET_SECRET,
});



//base64 ecoding and decoding
let base64 = {
  encode: function encode(buffer) {
    return buffer
      .toString("base64")
      .replace(/\+/g, "-") // Convert '+' to '-'
      .replace(/\//g, "_") // Convert '/' to '_'
      .replace(/=+$/, ""); // Remove ending '='
  },
  decode: function decode(base64) {
    // Add removed at end '='
    base64 += Array(5 - base64.length % 4).join("=");
    base64 = base64
      .replace(/\-/g, "+") // Convert '-' to '+'
      .replace(/\_/g, "/"); // Convert '_' to '/'
    return new Buffer(base64, "base64");
  },
  validate: function validate(base64) {
    return /^[A-Za-z0-9\-_]+$/.test(base64);
  }
};

//get message lang specific
let message = (code) => {
  var m = require("../core/message");
  if (!code in m || isNaN(code)) {
    code = 1001;
  }
  var msg = m[code];
  if (!msg) {
    msg = m[1001];
  }
  return msg;
};

//send error
let error = (res, code, data, send) => {
  console.log("code returned " + code);
  if (!send && send != 0) {
    send = 1;
  }

  if (!data) {
    data = [];
  }

  if (isNaN(code) || code == 0) {
    code = 1001;
  }

  var err = message(code);

  errtxt = {
    code: code,
    message: err.message,
    data: data
  };

  if (send == 1) {
    res.statusCode = err.httpCode;
    res.json(errtxt);
    res.end();
    logger('', errtxt, data);
  } else {
    return Promise.reject(errtxt);
  }
};

//send success response
let success = (res, data, code) => {
  if (!data) {
    data = [];
  }
  if (code > 0) {
    var m = message(code);
    res.json({
      code: 0,
      message: m.message,
      data: data
    });
  } else {
    res.json(data);
  }
  //res.json({ code: 0, message: message, data: data });
};

//validate request param
let paramValidate = () => {
  let resCode = 0;

  for (var i = 0; i < arguments.length; i++) {
    if (arguments[i].val) {
      resCode = arguments[i].code;
      break;
    }
  }

  if (resCode > 0) {
    return Promise.reject(resCode);
  }
  return Promise.resolve();
};

//check is email valide or not
let isValidEmail = (email) => {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};



let token = {
  get: (user) => {
    let jwt = require("jsonwebtoken");
    return jwt.sign(user, process.env.JWT);
  },
  verify: (req) => {
    try {
      let jwt = require("jsonwebtoken");

      //read header
      let auth = req.headers["authorization"];
      if (!auth) {
        return Promise.reject();
      }
      let token = auth.split(/\s+/).pop();

      //
      let user = jwt.verify(token, process.env.JWT);

      if (!user || !user.accessId) {
        return Promise.reject(401);
      }
      return Promise.resolve(user);
    } catch (e) {
      return Promise.reject(e);
    }
  },
  TTL: () => {
    //one week token
    //mili*sec*hour*hour in day*no of day
    return 1000 * 60 * 60 * 24 * 7;
  }
};

//generate random Password
let generatePassword = () => {
  var length = 8,
    charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
    retVal = "";
  for (var i = 0, n = charset.length; i < length; ++i) {
    retVal += charset.charAt(Math.floor(Math.random() * n));
  }
  return retVal;
};


const moongooseError = (err, res) => {
  let msg = "";
  let status = 1001;

  if (err) {
    if (err.name && err.name == 'ValidationError') {
      for (field in err.errors) {
        msg += err.errors[field].message + "_";
        status = 1002;
      }
      // return response;
    } else {
      msg = err.constructor == String ? err : "Something went wrong!";
      // return response;
    }
  } else {
    msg = 'Something went wrong!';
    // return response;
  }

  error(res, status, response);

}


// let resizeImage = (filePath,size) =>{
//   // return new Promise((res,rej)=>{
//   //   gm(filePath).resize().toBuffer
//   // })
//   // gm(filePath).resize(size).toBuffer()
//   // .then((buffer)=>{
//   //   console.log("buffer",buffer);
//   //   return Promise.resolve(buffer);
//   // })  
//   var resizeImg = bluebird.promisify(function(input, size, cb) {
//   gm(input).resize(size).toBuffer(function(err, buffer) {
//     if (err) cb(err); else cb(null, buffer);
//   });
// });
// }

let uploadImgS3 = async (key, path) => {
  let response = await s3.upload({
    Key: key, //id + "_" + file.originalFilename,
    Bucket: "floor-index",
    // ACL:"public-read",
    Body: fs.createReadStream(path)
  }).promise();
  return response;
}


let requestUrl = (url) => {
  return new Promise(function (resolve, reject) {
    request(url, (error, response, body) => {
      // in addition to parsing the value, deal with possible errors
      if (error) return reject(err);
      try {
        // JSON.parse() can throw an exception if not valid JSON
        resolve(JSON.parse(body));
      } catch (e) {
        reject(e);
      }
    });
  })
}


let logger = (infoType, msg, rest) => {
  switch (infoType) {
    case 'info':
      winstonLogger.info(`${msg} ${rest}`);
    default:
      winstonLogger.error(`${msg} ${rest}`);
  }
}

const helper = {
  base64,
  error,
  success,
  paramValidate,
  isValidEmail,
  token,
  generatePassword,
  moongooseError,
  // resizeImage,
  uploadImgS3,
  requestUrl,
  logger
};

module.exports = helper;