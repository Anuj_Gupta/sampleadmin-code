const winston = require('winston');
const getElasticInstanceLogger = require('../lib/database/elasticsearch/index');
const winston_elasticsearch = require('winston-elasticsearch');

let logger = winston.createLogger({ exitOnError: false });

logger.add(new winston_elasticsearch({
    client:getElasticInstanceLogger(),
    index: "floor-index-logger"
}));

module.exports = logger;
