const helper = require("../core/helper");
const noAuthURL = [
  "/",
];

module.exports = (app) => {
  let auth = (req, res, next) => {
    Promise.resolve()
      .then(() => {
        c = (
          !noAuthURL.includes(req.originalUrl)
          // && req.originalUrl.search("faq") == -1 
        );
        if (c) {
          return helper.token.verify(req);
        }
        return Promise.reject(false);
      })
      .then(d => {
        req.uSession = d;
        next();
      })
      .catch(err => {
        if (err === false) next();
        else helper.error(res, 401);
      });
  };

  app.use(auth);
};
