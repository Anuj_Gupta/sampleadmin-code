const express = require("express");
const helper = require("../../core/helper");
const propertyInstance = require('./property.model');
const propertyRouter = express.Router();
const bodybuilder = require('bodybuilder');
const multiparty = require("multiparty");
const bluebird = require('bluebird');
const fs = require('fs');
let property = {};
let mongoose = require('mongoose');



property.get = (req, res) => {
  let brokerId = req.uSession._id;
  let reqQuery = req.query;
  let queryElastic = bodybuilder();
  let from = reqQuery.page && reqQuery.limit ? (reqQuery.page - 1) * reqQuery.limit : 0;
  let size = reqQuery.limit ? reqQuery.limit : 20;
  let orderBy = reqQuery.orderBy && reqQuery.orderBy.split(',');
  orderBy = orderBy && orderBy.length == 2 ? orderBy : ['createdAt', 'desc'];

  //Must match with broker id first
  queryElastic.query('match', 'brokerId', brokerId);
  queryElastic.from(from)
  queryElastic.size(size);
  queryElastic.sort(orderBy[0], orderBy[1]);

  Object.keys(reqQuery).map((param) => {
    switch (param) {
      case 'rent':
        let ranges = reqQuery[param].split(',').map(Number);
        queryElastic.query('range', param, { gte: ranges[0], lte: ranges[1] });
        break;

      case 'roomType':
      case 'bhkType':
      case 'bedLocation':
      case 'availabilty':
      case 'noOfAvailabilty':
      case 'facing':
        queryElastic.filter('terms', param, reqQuery[param].split(',').map(Number));
        break;

      case 'city':
        let city = reqQuery[param].split(',');
        if (city && city.length > 0) {
          city.forEach(element => {
            queryElastic.filter('match_phrase', 'locationDetails.city', element);
          });
        }
        break;

      case '_id':
        queryElastic.query('match', '_id', reqQuery[param]);
        break;

      case 'propertyTitle':
        queryElastic.filter('wildcard', 'propertyTitle', `${reqQuery[param]}*`);
        break;

    }

  })

  queryElastic = queryElastic.build();
  console.log(JSON.stringify(queryElastic));

  propertyInstance.esSearch(queryElastic, { hydrate: true })
    .then((data) => {
      // console.log(data)
      let propertyList = data.hits.hits ? data.hits.hits : [];
      let total = data.hits.total ? data.hits.total : {};
      helper.success(res, { count: propertyList.length, total: total, propertyList: propertyList }, 2005);
    })
    .catch((err) => {
      console.log(err);
      helper.error(res, 1001, err);
    })
}

property.upload = async (req, res) => {
  let reqBody = {};
  let unlinkFile = [];

  return parseImageAndUpload(req, res)
    .then(async (resp) => {
      if (!resp)
        return Promise.reject(false);

      reqBody = resp.reqB ? resp.reqB : {};
      if (resp.files && resp.files.length > 0) {
        console.log('Got Image file ', resp.files.length);
        reqBody.hasImage = true;
        return bluebird.all(resp.files);
      }
      else
        return Promise.resolve(true);
    })
    .then((images) => {

      if (reqBody.hasImage) {
        //loop over file response and delete file from temp file
        images.map((image) => {
          image.map((ob) => {
            if (ob.path) {
              // fs.unlink(ob.path);
              unlinkFile.push(ob.path);
            } else {
              reqBody.s3BucketImg.push(ob);
            }
          });
        })
      }
      return Promise.resolve(true);
    })
    .then(() => {
      // let property = new propertyInstance(reqBody);
      return reqBody.save()
    })
    .then((resp) => {
      helper.success(res, resp, 2001)
    })
    .catch((err) => {
      helper.error(res, 1001, err);
    })
    .then(() => {
      if (unlinkFile.length > 0) {
        return Promise.all(unlinkFile.map(un => fs.unlink(un, () => { console.log('Removed') })))
          .then((isDone) => {
            console.log(isDone);
          })
      }
    });
}

property.put = (req, res) => {
  let reqBody = {};
  let unlinkFile = [];
  return parseImageAndUpload(req, res)
    .then((resp) => {
      if (!resp)
        return Promise.reject(false);

      reqBody = resp.reqB ? resp.reqB : {};
      if (resp.files && resp.files.length > 0) {
        console.log('Got Image file ', resp.files.length);
        reqBody.hasImage = true;
        return bluebird.all(resp.files);
      }
      else
        return Promise.resolve(true);
    })
    .then((images) => {

      if (reqBody.hasImage) {
        //loop over file response and delete file from temp file
        images.map((image) => {
          image.map((ob) => {
            if (ob.path) {
              unlinkFile.push(ob.path);
            } else {
              reqBody.s3BucketImg.push(ob);
            }
          });
        })
      }
      return Promise.resolve(true);
    })
    .then(() => {
      let id = reqBody._id;
      delete reqBody._id;
      return propertyInstance.findByIdAndUpdate({ _id: id }, reqBody, { new: true })
    })
    .then((resp) => {
      helper.success(res, resp)
    })
    .catch((err) => {
      console.log(err);
      helper.error(res, 1001, err);
    })
    //Unlink file from disk
    .then(() => {
      if (unlinkFile.length > 0) {
        return Promise.all(unlinkFile.map(un => fs.unlink(un, () => { console.log('Removed') })))
          .then((isDone) => {
            console.log(isDone);
          })
      }
    });
}


property.delete = (req, res) => {
  let bId = req.uSession._id;
  let propertyId = req.params.id;
  let query = { "_id": mongoose.Types.ObjectId(propertyId), brokerId: mongoose.Types.ObjectId(bId) };
  return propertyInstance.update(query, { isActive: false }, { new: true }).exec()
    .then((resp) => {
      if (resp && resp.nModified && resp.nModified > 0)
        helper.success(res, resp, 2003);
      else
        helper.success(res, {}, 2004);
    })
    .catch((err) => {
      helper.error(res, 1001, err);
    });


}

property.searchLocality = (req, res) => {
  let query = req.query;
  let key = JSON.parse(process.env.GOOGLE_PLACE_KEY);
  let placeUrl = process.env.GOOGLE_PLACE_API;

  let randomKey = key[Math.floor(Math.random() * key.length)];

  let url = `${placeUrl}key=${randomKey}&input=${query.search}`;

  helper.requestUrl(url)
    .then((body) => {
      helper.success(res, body);
    })
    .catch((err) => {
      helper.error(res, 1001, err);
    });
}

property.placeDetail = (req, res) => {
  let id = req.params.id;
  let key = JSON.parse(process.env.GOOGLE_PLACE_KEY);
  let placeDetailUrl = process.env.GOOGLE_PLACE_DETAILS;

  let randomKey = key[Math.floor(Math.random() * key.length)];

  let url = `${placeDetailUrl}placeid=${id}&key=${randomKey}`;
  helper.requestUrl(url)
    .then((body) => {
      helper.success(res, body);
    })
    .catch((err) => {
      helper.error(res, 1001, err);
    });
}


property.publishProperty = (req, res) => {
  let body = req.body;
  let published = [];
  let unpublished = [];
  let brokerId = req.uSession._id;
  return Promise.resolve()
    .then(() => {
      published = body.published;
      unpublished = body.unpublished;
      if (!published || !unpublished)
        return Promise.reject('Require published or unpublished');
      else
        return Promise.resolve(true);
    })
    .then(() => {
      let promises = [];

      //Published array
      if (published.length > 0) {
        let p = published.map(pp => mongoose.Types.ObjectId(pp));
        let pquery = { "_id": { "$in": p }, "brokerId": mongoose.Types.ObjectId(brokerId) };
        let pcondition = { isPublished: true };
        promises.push(propertyInstance.update(pquery, pcondition));
      }

      //UnPublished array
      if (unpublished.length > 0) {
        let upu = unpublished.map(up => mongoose.Types.ObjectId(up));
        let uquery = { "_id": { "$in": upu }, "brokerId": mongoose.Types.ObjectId(brokerId) };
        let ucondition = { isPublished: false };
        promises.push(propertyInstance.update(uquery, ucondition));
      }

      return Promise.all(promises);
    })
    .then((resolvedPr) => {
      if (resolvedPr) {
        helper.success(res, resolvedPr);
      } else {
        helper.error(res, 1001);
      }
    })
    .catch((err) => {
      helper.error(res, 1001, err);
    })
}

let extractFields = (fields, brokerId) => {
  let reqBody = {};
  if (fields) {
    if (fields._id && fields._id.length > 0) {
      reqBody._id = fields._id[0]
    }

    reqBody.hasImage = false;
    reqBody.s3BucketImg = fields.s3BucketImg && fields.s3BucketImg.length > 0 && fields.s3BucketImg instanceof Array ? JSON.parse(fields.s3BucketImg[0]) : [];
    if (!reqBody.s3BucketImg) {
      reqBody.s3BucketImg = [];
    }

    reqBody.brokerId = brokerId;
    reqBody.propertyTitle = fields.propertyTitle && fields.propertyTitle.length > 0 ? fields.propertyTitle[0] : "";

    reqBody.propertyInfo = fields.propertyInfo && fields.propertyInfo.length > 0 && fields.propertyInfo instanceof Array ? JSON.parse(fields.propertyInfo[0]) : [];

    // reqBody.location = fields.location && fields.location.length > 0 ? fields.location[0] : "";
    // reqBody.address = fields.address && fields.address.length > 0 ? fields.address[0] : "";

    reqBody.locationDetails = fields.locationDetails && fields.locationDetails.length > 0 && fields.locationDetails instanceof Object ? JSON.parse(fields.locationDetails[0]) : {};

    reqBody.roomType = fields.roomType && fields.roomType.length > 0 ? fields.roomType[0] : 3;
    reqBody.bhkType = fields.bhkType && fields.bhkType.length > 0 ? fields.bhkType[0] : 2;
    reqBody.noOfAvailabilty = fields.noOfAvailabilty && fields.noOfAvailabilty.length > 0 ? fields.noOfAvailabilty[0] : 1;
    reqBody.bedLocation = fields.bedLocation && fields.bedLocation.length > 0 ? fields.bedLocation[0] : 1;
    reqBody.availabilty = fields.availabilty && fields.availabilty.length > 0 ? fields.availabilty[0] : 1;
    reqBody.rent = fields.rent && fields.rent.length > 0 ? fields.rent[0] : 0;
    reqBody.deposit = fields.deposit && fields.deposit.length > 0 ? fields.deposit[0] : 0;
    reqBody.furnisedType = fields.furnisedType && fields.furnisedType.length > 0 ? fields.furnisedType[0] : 0;
    reqBody.tenants = fields.tenants && fields.tenants.length > 0 ? fields.tenants[0] : 1;
    reqBody.facing = fields.facing && fields.facing.length > 0 ? fields.facing[0] : 0;
    reqBody.propertySize = fields.propertySize && fields.propertySize.length > 0 ? fields.propertySize[0] : 0;
    reqBody.isPublished = fields.isPublished && fields.isPublished.length > 0 ? fields.isPublished[0] : 0;
    return reqBody;
  }
}

let parseImageAndUpload = (req, res) => {
  return new Promise((resolve, reject) => {
    new multiparty.Form().parse(req, (err, fields, files) => {
      let reqBody = extractFields(fields, req.uSession._id);
      //INFO:Since S3 bucket folder will be created based on property id
      if (reqBody && !reqBody._id) {
        reqBody = new propertyInstance(reqBody);
      }

      let filesPromises;
      if (files && files.images) {
        filesPromises = files.images.map((file, i) => {
          if (!file.headers || !['image/jpeg', 'image/png', 'image/jpg'].includes(file.headers["content-type"]))
            return Promise.reject('Invalid File format');

          return bluebird.join(file, helper.uploadImgS3(`${reqBody._id}/${new Date().getTime()}${i}.jpg`, file.path));
        });
      }
      resolve({ files: filesPromises, reqB: reqBody });
    })
  })
    .catch((err) => {
      console.log(err);
      helper.error(res, 1001, err);
    })
}




module.exports = (app, uri) => {
  propertyRouter.get('/', property.get);
  propertyRouter.post('/', property.upload);
  propertyRouter.put('/', property.put);
  propertyRouter.delete('/:id', property.delete);
  propertyRouter.get('/searchLocality', property.searchLocality);
  propertyRouter.get('/placeDetail/:id', property.placeDetail);
  propertyRouter.post('/publishProperty', property.publishProperty);
  app.use(uri, propertyRouter);
};


