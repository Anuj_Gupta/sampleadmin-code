'use strict';
/**
 * Module dependencies.
 */
const getElasticInstance  = require('../../lib/database/elasticsearch/index');
const Promise = require("bluebird");

let mongoose = require('mongoose'),
    mongoosastic = require('mongoosastic'),
    Schema = mongoose.Schema;

/**
 * Property Type Schema
 */

// INFO: ENUM

/* DES: roomType 
 
 *   1 - PG-Boys
 *   2 - PG-Girls
 *   3 - Rent
 *   4 - PG-Girls/Boys
 * */

/* DES: bedLocation 
 
*   1 - BEDROOM
*   2 - BEDROOM/HALL
*   3 - FULL APARTMENT
*   4 - HALL
* */


/* DES: availabilty 
 
*   1 - ALL
*   2 - GIRLS
*   3 - BOYS
*   4 - FAMILY
*   5 - CORPORATE
* */

/* DES:  bhkType
 
*   1 - 1RK
*   2 - 1BHK
*   3 - 2BHK
*   4 - 3BHK
*   5 - 4+BHK
* */


/* DES:  furnisedType

*   0 - NONE 
*   1 - FULLY FURNISHED
*   2 - SEMI-FURNISED
*   3 - UNFURNISHED
* */


/* DES:  tenants
 
*   1 - DOESNT'T MATTER
*   2 - FAMILY
*   3 - BACHELORS
*   4 - COMPANY
* */

let propertySchema = new Schema({
    propertyTitle: { type: String, trim: true, es_type: 'text' },
    // location: { type: String, es_type: 'text', require: [true, 'location require'], trim: true },
    // address: { type: String, es_type: 'text', require: [true, 'address require'], trim: true },
    locationDetails: {
        city: { type: String, trim: true, es_type: 'text' },
        locality: { type: Object, es_type: 'multi_field' },
        area: { type: String, trim: true, es_type: 'text' }
    },
    bhkType: {
        type: Number,
        require: [true, 'BHK type require'],
        min: 1,
        max: 4,
        default: 2,
    },
    roomType: {
        type: Number,
        require: [true, 'roomType require'],
        min: 1,
        max: 4,
        default: 3,
    },
    bedLocation: {
        type: Number,
        require: [true, 'bedlocation require'],
        min: 1,
        max: 4,
        default: 1
    },
    availabilty: {
        type: Number,
        require: [true, 'availabilty required'],
        min: 1,
        max: 5,
        default: 1,
    },
    facing: {
        type: Number,
        min: 0,
        max: 8,
        default: 0
    },
    propertySize: {
        type: Number,
        required: [true, 'property size required']
    },
    noOfAvailabilty: {
        type: Number,
        required: [true, 'No Of Availabilty require']
    },
    rent: {
        type: Number,
        require: [true, 'rent require']
    },
    deposit: {
        type: Number,
        default: 0
    },
    furnisedType: {
        type: Number,
        min: 0,
        max: 3,
        default: 0,
        required: [true, 'Furnised type required']
    },
    tenants: {
        type: Number,
        min: 1,
        max: 4,
        default: 1,
    },
    brokerId: { type: Schema.Types.ObjectId, es_type: 'text', ref: 'usersInfo', required: [true, 'Broker Id required'] },
    // brokerId: { type: Schema.Types.ObjectId, ref: 'usersInfo', required: [true, 'Broker Id required'], es_type: 'nested', es_include_in_parent: true },

    s3BucketImg: [{
        Location: { type: String, es_type: 'text', default: "" },
        "_id": false
    }],
    isPublished: { type: Boolean, default: false },
    propertyInfo: {
        AC: { type: Boolean },
        hasKitchen: { type: Boolean },
        neareastStation: { type: String, es_type: 'text' },
        stationInKM: { type: Number, },
        neareastMetro: { type: String, es_type: 'text' },
        metroInKM: { type: Number, },
        nearestMarket: { type: String, es_type: 'text' },
        marketInKM: { type: Number, },
        waterFacility: { type: Boolean },
        wifi: { type: Boolean },
        TV: { type: Boolean },
        electricityRent: { type: Boolean, default: false },
        securityAvailable: { type: Boolean, default: false },
        CCTV: { type: Boolean },
        geyser: { type: Boolean },
        visitorParking: { type: Boolean }
    },
    isActive: { type: Boolean, default: true },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: null
    }
});

propertySchema.pre('update', function (next) {
    let property = this;
    property._update['updatedAt'] = new Date();
    next();
});

propertySchema.pre('findByIdAndUpdate', function (next) {
    let property = this;
    property._update['updatedAt'] = new Date();
    next();
});



propertySchema.plugin(mongoosastic, {
    index: 'floor-index',
    esClient: getElasticInstance()
});

let property = module.exports = mongoose.model('properties', propertySchema, 'properties');

property.search = Promise.promisify(property.search, { context: property });
property.esSearch = Promise.promisify(property.esSearch, { context: property });

// property.createMapping({
//     "analysis": {
//         "analyzer": {
//             "analyzer_case_insensitive": {
//                 "tokenizer": "keyword",
//                 "filter": "lowercase"
//             }
//         }
//     }
// }, function (err, mapping) {
//     // do neat things here
//     console.log(mapping);
//     console.log(err);
// });

// property.createMapping(function (err, mapping) {
//     if (err) {
//         console.log('error creating mapping (you can safely ignore this)');
//         console.log(err);
//     } else {
//         console.log('mapping created!');
//         console.log(mapping);
//     }
// });



// module.exports = property