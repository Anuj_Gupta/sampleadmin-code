let elasticsearch = require('elasticsearch');
let elasticClient;
let elasticLogger;

module.exports = getElasticInstance = (config) => {
    if (elasticClient)
        return elasticClient;
    elasticClient = new elasticsearch.Client({
        // host: process.env.ELASTIC_SEARCH_Url,
        host: process.env.ELASTIC_SEARCH_Local,
        log: 'debug'
    });
    return elasticClient;
};


module.exports = getElasticInstanceLogger = () => {
    if (elasticLogger)
        return elasticLogger;
    elasticLogger = new elasticsearch.Client({
        // host: process.env.ELASTIC_SEARCH_Url,
        host: process.env.ELASTIC_SEARCH_Local,
        log: 'info',
    });
    return elasticLogger;
};
