
module.exports = function (app){
  require('./mongodb')(app)
  require('./elasticsearch')(app)
}