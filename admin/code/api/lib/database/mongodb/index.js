//for mongo atlas
// let url = `mongodb://root:${process.env.DB_PASS}@${process.env.DB_HOST}/admin?replicaSet=Sds-shard-0&ssl=true`;
// const Mongo = require('mongodb');
// const MongoClient = Mongo.MongoClient;

// let db = {};
// let dbCon = null;
// let helper = require('../../../core/helper');



const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

module.exports = dbConnect = (config) => {
    //Database Connection Url
    // let uri = 'mongodb://'+config.database.host+":"+ config.database.port + "/" + config.database.databaseName;
    // let options = {
    //     user: config.database.userName,
    //     pass: config.database.password,
    //     useMongoClient: true
    // };
    //Connection Establishment 
    // options
    mongoose.connect(process.env.DB_HOST)
    .then(()=>{
        console.log('Connected database');
    })
    .catch((err)=>{
        console.log('Error at mongo ', err);
    })
    //mongoose.connect(uri, {useMongoClient: true});
}