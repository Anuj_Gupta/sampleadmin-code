import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { UserFlowService,CommonService } from '../../shared/shared.module';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private userFlowService: UserFlowService,private commonService:CommonService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      // mobile: ['9022171228', [Validators.required, Validators.pattern('^[6-9]{1}\\d{9}$')]],
      // password: ['test@123', Validators.required],
      mobile: ['', [Validators.required, Validators.pattern('^[6-9]{1}\\d{9}$')]],
      password: ['', Validators.required],

    });
  }

  brokerLogin(e: any) {
    console.log(this.loginForm.value);
    if (this.loginForm.valid) {
      this.userFlowService.brokerLogin(this.loginForm.value)
        .subscribe(res => {
          console.log('Response ', res);
          this.commonService.showToast('Done',1);
          this.userFlowService.setBrokerDetails(res);
        })
    }
  }

}
