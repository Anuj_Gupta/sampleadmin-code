import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserFlowRoutingModule } from './user-flow.routing.module';
import { RouterModule } from '@angular/router';


//Component
import { LoginComponent } from './login/login.component';

//Module
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [LoginComponent],
  imports: [
    UserFlowRoutingModule,
    RouterModule,
    CommonModule,
    SharedModule,
  ]
})
export class UserFlowModule { }
