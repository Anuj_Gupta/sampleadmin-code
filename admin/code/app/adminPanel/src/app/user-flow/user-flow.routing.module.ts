import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NoAuthGuard } from '../shared/shared.module';


import { LoginComponent } from './login/login.component'



const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
        canActivate:[NoAuthGuard]
    },
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login'
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UserFlowRoutingModule { }
