import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';


//Interceptor
import { RequestInterceptor } from './request.interceptor';
const httpInterceptor = { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true };



//Auth Guard
import { AuthGuard } from './guards/auth/auth.guard';
import { NoAuthGuard } from './guards/auth/no-auth.guard';
const guards = [AuthGuard, NoAuthGuard];


//Component 
import { MainLayoutComponent } from './component/main-layout/main-layout.component';
import { LoaderComponent } from './component/loader/loader.component';
import { FileUploadComponent } from './component/file-upload/file-upload.component';
const component = [MainLayoutComponent, LoaderComponent, FileUploadComponent];


//Services
import { UserFlowService } from './services/user-flow/user-flow.service';
import { CommonService } from './services/common/common.service';
import { PropertyService } from './services/property/property.service';
const services = [UserFlowService, CommonService, PropertyService];


const exportModule = [CommonModule, MainLayoutComponent, MDBBootstrapModule, ReactiveFormsModule, FormsModule, LoaderComponent,FileUploadComponent];

@NgModule({
  declarations: [...component],
  imports: [
    RouterModule,
    MDBBootstrapModule.forRoot(),
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [...services, httpInterceptor, ...guards],
  exports: [...exportModule],
  schemas: [NO_ERRORS_SCHEMA]

})
export class SharedModule { }
export { MainLayoutComponent, LoaderComponent, FileUploadComponent }
export { UserFlowService, PropertyService, CommonService }
export { AuthGuard, NoAuthGuard }