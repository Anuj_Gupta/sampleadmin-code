import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class UserFlowService {

  // brokerDetails: any;

  constructor(private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute) { }

  brokerLogin(body): Observable<any> {
    return this.http.post("user/otpRequest", body);
  }

  setBrokerDetails(brokerData) {
    if (brokerData.data.AES) {
      let brokerDetails = this.parseJwt(brokerData.data.AES);
      localStorage.setItem('floorIndexBrokerToken', brokerData.data.AES);
      debugger;
      localStorage.setItem('floorIndexBroker', btoa(JSON.stringify(brokerDetails)));
      this.router.navigate(['/dashboard'], { relativeTo: this.activatedRoute });
    }
  }

  isAuthenticated() {
    let brokerDetails = this.getUserDetails();
    return brokerDetails && brokerDetails["_id"] ? true : false
  }

  getToken(){
      return localStorage.getItem('floorIndexBrokerToken');
  }

  logout(){
    localStorage.removeItem('floorIndexBrokerToken');
    localStorage.removeItem('floorIndexBroker');
  }

  parseJwt(token) {
    let base64Url = token.split('.')[1];
    let base64 = decodeURIComponent(atob(base64Url).split('').map((c) => {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(base64);
  }

  getUserDetails() {
    let brokerDetails = { _id: "" };
    if (localStorage.getItem('floorIndexBroker')) {
      brokerDetails = JSON.parse(atob(localStorage.getItem("floorIndexBroker")));
    }
    return brokerDetails;
  }
}
