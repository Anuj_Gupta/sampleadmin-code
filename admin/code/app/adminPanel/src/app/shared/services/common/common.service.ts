import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { NotyfService } from 'ng-notyf';


let subject = new Subject();

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  commonAction = subject;
  constructor(private notyfService: NotyfService) {
    this.toastConfig();
  }

  private toastConfig() {
    this.notyfService.toastDelay = 6000;
    this.notyfService.toastContainerStyle = { 'top': '0','height': '10px' };
    this.notyfService.toastStyle = { 'color': 'white', 'font-size': '18px', 'font-weight': '700' };
  }


  showToast(text = "", type = 1) {
    //1 - success
    //0 - fail

    // this.toastConfig();
    if (type == 0) {
      let msg = text ? text : 'Something went wrong!'
      this.notyfService.error(msg);
    }
    if (type == 1) {
      this.notyfService.success(text);
    }
  }



  getCommonSubject(): Observable<any> {
    return this.commonAction.asObservable();
  }

  sendCommonSubject(action, data) {
    this.commonAction.next({ action: action, data: data });
  }
}
