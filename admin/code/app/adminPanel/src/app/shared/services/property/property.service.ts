import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  constructor(private http: HttpClient) { }

  getPropertyList(query: string): Observable<any> {
    return this.http.get(`property${query}`);
  }

  getPropertyByQuery(query: any) {
    return this.http.get(`property?${query}`)
  }

  addProperty(formData): Observable<any> {
    return this.http.post('property', formData);
  }

  updateProperty(formData): Observable<any> {
    return this.http.put('property', formData);
  }

  getLocalityGoogle(query: string): Observable<any> {
    return this.http.get(`property/searchLocality?search=${query}`);
  }

  getPlaceDetail(placeId: string): Observable<any> {
    return this.http.get(`property/placeDetail/${placeId}`);
  }

  changeStatus(body: any): Observable<any> {
    return this.http.post('property/publishProperty', body);
  }
}
