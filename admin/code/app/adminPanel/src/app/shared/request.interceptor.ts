import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { tap } from "rxjs/operators";
import { UserFlowService } from '../shared/services/user-flow/user-flow.service';
import { environment } from '../../environments/environment';
import { CommonService } from '../shared/services/common/common.service';
@Injectable()
export class RequestInterceptor implements HttpInterceptor {
    baseUrl: String = '';
    userProfileUrl: String = '';

    constructor(private userFlowService: UserFlowService, private commonService: CommonService) {
        this.baseUrl = environment.url;
        this.userProfileUrl = environment.userProfile;
    }
    //function which will be called for all http calls
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let headers = request.headers;
        if (this.userFlowService.isAuthenticated()) {
            if (request.body && request.body.constructor == FormData) {
                headers = headers.set("Authorization", `Bearer ${this.userFlowService.getToken()}`);
                headers.delete("Content-Type");
            } else {
                headers = headers
                    .set("Authorization", `Bearer ${this.userFlowService.getToken()}`)
                    .set("Content-Type", "application/json");
            }
        }

        let updatedRequest;
        //how to update the request Parameters
        if (request.url.search('user') != -1)
            updatedRequest = request.clone({ url: this.userProfileUrl + request.url });
        else
            updatedRequest = request.clone({ url: this.baseUrl + request.url, headers: headers });

        //logging the updated Parameters to browser's console
        return next.handle(updatedRequest).pipe(
            tap(
                event => {
                    //logging the http response to browser's console in case of a success
                    if (event instanceof HttpResponse) {

                    }
                },
                error => {
                    // debugger;
                    //logging the http response to browser's console in case of a failuer
                    if (error instanceof HttpErrorResponse) {
                        // console.log("api call error :", error);

                        if (error.status === 401) {
                            // redirect to the login route
                            this.userFlowService.logout();
                            return;
                        }

                        if ([404].includes(error.status)) {
                            this.commonService.showToast('Url Not Found!', 0);
                        }


                        if ([0,500].includes(error.status)) {
                            let msg = error.error && error.error.msg ? error.error.msg : 'Something Went wrong!' 
                            this.commonService.showToast(msg, 0);
                        }

                        if (error.error.message != "") {
                            //document.getElementById("#error").innerHTML = event.error.message;
                        }

                    }
                }
            )
        );
    }
}