import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserFlowService } from '../../services/user-flow/user-flow.service';

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(private router: Router, private userFlowService: UserFlowService, private activatedRoute: ActivatedRoute) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    let isAuthenticated = this.userFlowService.isAuthenticated();

    if (isAuthenticated) {
      this.router.navigate(['/dashboard'], { relativeTo: this.activatedRoute });
    }
    return !isAuthenticated;
  }
}
