import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { UserFlowService } from '../../services/user-flow/user-flow.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private userFlowService: UserFlowService, private activatedRoute: ActivatedRoute) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let isAuthenticated = this.userFlowService.isAuthenticated();
    if (isAuthenticated) {
      return isAuthenticated;
    }
    // not logged in so redirect to login page with the return url
    // queryParams: { returnUrl: state.url }
    this.router.navigate(['/login'], { relativeTo: this.activatedRoute });
  }
}
