import { Component, OnInit, Input, HostListener } from '@angular/core';
import { CommonService } from '../../services/common/common.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  @Input('imagesUrl') imagesUrl: any[];

  errors: string[] = [];
  dragAreaClass: string = 'dragarea';
  fileExt: string = "JPG, JPEG, PNG";
  maxFiles: number = 20;
  maxSize: number = 5; // 5MB
  fileFormData: any[] = [];
  urls: any[] = [];
  imageFromParent: boolean = false;
  isImageUpload: boolean = false;
  
  constructor(private commonService: CommonService) { }

  ngOnInit() {

  }

  ngOnDestroy() {
    console.log('Destroying file-upload');
    this.urls = [];
    this.fileFormData = [];
    this.imageFromParent = false;
    this.isImageUpload = false;
  }

  @HostListener('dragover', ['$event']) onDragOver(event) {
    this.dragAreaClass = "droparea";
    event.preventDefault();
  }

  onFileChange(event) {
    let files = event.target.files;
    this.saveFiles(files);
  }

  @HostListener('dragenter', ['$event']) onDragEnter(event) {
    this.dragAreaClass = "droparea";
    event.preventDefault();
  }

  @HostListener('dragend', ['$event']) onDragEnd(event) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
  }

  @HostListener('dragleave', ['$event']) onDragLeave(event) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
  }

  @HostListener('drop', ['$event']) onDrop(event) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
    event.stopPropagation();
    let files = event.dataTransfer.files;
    this.saveFiles(files);
  }


  ngOnChanges() {
    if (this.imagesUrl && this.imagesUrl.length > 0 && this.urls.length == 0) {
      this.urls = this.imagesUrl;
      this.imageFromParent = true;
    }
  }

  saveFiles(files) {
    this.errors = []; // Clear error
    // Validate file size and allowed extensions
    if (files.length > 0 && (!this.isValidFiles(files))) {
      return false;
    }
    if (files.length > 0) {
      for (let j = 0; j < files.length; j++) {
        // formData.append("file[]", files[j], files[j].name);
        // console.log(files[j]);
        this.fileFormData.push(files[j]);
        var reader = new FileReader();
        reader.onload = (event: any) => {
          this.isImageUpload = true;
          this.urls.push({ Location: event.target.result });
        };
        reader.readAsDataURL(files[j]);
      }
      debugger;
      this.commonService.sendCommonSubject('imagesForm', this.fileFormData);
    }
  }


  isValidFiles(files) {
    // Check Number of files
    if (files.length > this.maxFiles) {
      this.errors.push("Error: At a time you can upload only " + this.maxFiles + " files");
      this.commonService.showToast(`At a time you can upload only ${this.maxFiles} files`, 3);
      return;
    }
    this.isValidFileExtension(files);
    return this.errors.length === 0;
  }

  isValidFileExtension(files) {
    // Make array of file extensions
    let extensions = (this.fileExt.split(','))
      .map(function (x) { return x.toLocaleUpperCase().trim() });
    for (let i = 0; i < files.length; i++) {
      // Get file extension
      let ext = files[i].name.toUpperCase().split('.').pop() || files[i].name;
      // Check the extension exists
      let exists = extensions.includes(ext);
      if (!exists) {
        this.errors.push("Error (Extension): " + files[i].name);
        // alert('Error (Extension):');
        this.commonService.showToast(`Error (Extension): ${ext}`, 3);
      }
      // Check file size
      this.isValidFileSize(files[i]);
    }
  }

  isValidFileSize(file) {
    let fileSizeinMB = file.size / (1024 * 1000);
    let size = Math.round(fileSizeinMB * 100) / 100; // convert upto 2 decimal place
    if (size > this.maxSize) {
      this.errors.push("Error (File Size): " + file.name + ": exceed file size limit of " + this.maxSize + "MB ( " + size + "MB )");
      this.commonService.showToast(`${file.name} exceed file size limit of ${this.maxSize} MB (${size} MB)`, 3);
    }
  }

  deleteImg(i: number) {
    this.urls.splice(i, 1);
    this.fileFormData.splice(i, 1);

    //INFO:Will keep track for s3Bucket
    if (this.imageFromParent) {
      if (this.urls && this.urls.length > 0) {
        const sitesUrl = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
        this.urls = this.urls.filter(href => href.Location.match(sitesUrl));
      }
      this.commonService.sendCommonSubject('s3BucketImg', this.urls);
    }
    //INFO:Will keep track for uploaded images
    if (this.isImageUpload) {
      this.commonService.sendCommonSubject('imagesForm', this.fileFormData);
    }
  }
}
