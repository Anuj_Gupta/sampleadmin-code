import { Component, OnInit } from '@angular/core';
import { UserFlowService } from '../../services/user-flow/user-flow.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(private userFlowService: UserFlowService) { }

  ngOnInit() {
  }

  logout() {
    this.userFlowService.logout();
  }

}
