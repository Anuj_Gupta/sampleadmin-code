import { Component, OnInit, } from '@angular/core';
import { PropertyEnum } from './types/property-enum';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { PropertyService, LoaderComponent, FileUploadComponent, CommonService } from '../../shared/shared.module';
import { Router, ActivatedRoute, Params } from "@angular/router";

@Component({
  selector: 'app-property-add',
  templateUrl: './property-add.component.html',
  styleUrls: ['./property-add.component.scss']
})
export class PropertyAddComponent implements OnInit {
  propertyForm: FormGroup;
  propertyEnum = PropertyEnum;
  activeProperty: number;
  completedStage: string[] = [];
  activeShake: string;
  callingApi: boolean;
  localitySet: string[] = [];
  localitySelected: any = {};
  s3BucketImg: any[] = [];

  constructor(private fb: FormBuilder, private propertyService: PropertyService, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.propertyFormInit();
    this.getRouterParams();
  }

  ngAfterViewInit() {
    this.commonService.getCommonSubject().subscribe(res => {
      switch (res.action) {
        case 'imagesForm':
          this.propertyForm.patchValue({ 'images': res.data });
          break;
        case 's3BucketImg':
          this.propertyForm.patchValue({ 's3BucketImg': res.data });
          break;
      }
    })
  }

  editPropertyForm(property: any) {
    this.localitySelected = property.locationDetails.locality ? property.locationDetails.locality : {};

    this.propertyForm.patchValue({
      _id: property._id,
      propertyTitle: property.propertyTitle,
      roomType: property.roomType,
      bedLocation: property.bedLocation,
      availabilty: property.availabilty,
      noOfAvailabilty: property.noOfAvailabilty,
      bhkType: property.bhkType,
      propertySize: property.propertySize,
      facing: property.facing ? property.facing : 0,
      locationDetails: {
        city: property.locationDetails.city ? property.locationDetails.city : "",
        locality: this.localitySelected && this.localitySelected.formatted_address ? this.localitySelected.formatted_address : "",
        area: property.locationDetails.area ? property.locationDetails.area : ""
      },
      rent: property.rent ? property.rent : 0,
      deposit: property.deposit ? property.deposit : 0,
      furnisedType: property.furnisedType ? property.furnisedType : 1,
      tenants: property.tenants ? property.tenants : 1,
      images: [],
      s3BucketImg: property.s3BucketImg ? property.s3BucketImg : [],
      isPublished: false,
      propertyInfo: {
        AC: property.propertyInfo.AC,
        hasKitchen: property.propertyInfo.hasKitchen,
        neareastStation: property.propertyInfo.neareastStation,
        stationInKM: property.propertyInfo.stationInKM,
        neareastMetro: property.propertyInfo.neareastMetro,
        metroInKM: property.propertyInfo.metroInKM,
        nearestMarket: property.propertyInfo.nearestMarket,
        marketInKM: property.propertyInfo.marketInKM,
        waterFacility: property.propertyInfo.waterFacility,
        wifi: property.propertyInfo.wifi,
        TV: property.propertyInfo.TV,
        electricityRent: property.propertyInfo.electricityRent,
        securityAvailable: property.propertyInfo.securityAvailable,
        CCTV: property.propertyInfo.CCTV,
        geyser: property.propertyInfo.geyser,
        visitorParking: property.propertyInfo.visitorParking
      }
    });
    console.log(this.propertyForm.value);

  }

  getRouterParams() {
    this.activatedRoute.params.subscribe((params: Params) => {
      let propertyId = params["propertyId"];
      if (propertyId) {
        let query = `_id=${propertyId}`;
        this.propertyService.getPropertyByQuery(query).subscribe((res: any) => {
          let property = res && res.data && res.data.propertyList && res.data.propertyList.length > 0 ? res.data.propertyList[0] : {};
          this.editPropertyForm(property);
        });
      }
    });
  }

  propertyFormInit() {
    this.propertyForm = this.fb.group({
      _id: [''],
      propertyTitle: ['', [Validators.required]],
      roomType: [1, [Validators.required]],
      bedLocation: [1, [Validators.required]],
      availabilty: [1, [Validators.required]],
      noOfAvailabilty: [1],
      bhkType: [1, [Validators.required]],
      propertySize: ['', [Validators.required]],
      facing: [''],
      locationDetails: this.fb.group({
        city: ['Mumbai', [Validators.required]],
        locality: ['', [Validators.required]],
        area: ['', [Validators.required]]
      }),
      rent: ['', [Validators.required]],
      deposit: ['', [Validators.required]],
      furnisedType: [1, [Validators.required]],
      tenants: [1, [Validators.required]],
      images: ['', [Validators.required]],
      s3BucketImg: [],
      isPublished: [false],
      propertyInfo: this.fb.group({
        AC: [false],
        hasKitchen: [false],
        neareastStation: [''],
        stationInKM: [''],
        neareastMetro: [''],
        metroInKM: [''],
        nearestMarket: [''],
        marketInKM: [''],
        waterFacility: [false],
        wifi: [false],
        TV: [false],
        electricityRent: [false],
        securityAvailable: [false],
        CCTV: [false],
        geyser: [false],
        visitorParking: [false]
      })
    });
    this.activeProperty = 1;
    this.callingApi = false;
  }

  noOfAvailabiltyChange(e: any) {
    this.propertyForm.patchValue({ noOfAvailabilty: e.target.value });
  }

  savePropertyDetails(indexNumber) {

    // if (this.propertyForm.valid) {
    let propertyFD = new FormData();
    let _id = this.propertyForm.get("_id").value;

    let lD = this.propertyForm.get("locationDetails").value;
    lD.locality = this.localitySelected;

    let images = this.propertyForm.get("images").value;
    let imagesLength = images.length;

    let s3BucketImg = this.propertyForm.get('s3BucketImg').value;

    let pD = this.propertyForm.get('propertyInfo').value;


    propertyFD.append("propertyTitle", this.propertyForm.get("propertyTitle").value);
    propertyFD.append("roomType", this.propertyForm.get("roomType").value);
    propertyFD.append("bedLocation", this.propertyForm.get("bedLocation").value);
    propertyFD.append("availabilty", this.propertyForm.get("availabilty").value);
    propertyFD.append("noOfAvailabilty", this.propertyForm.get("noOfAvailabilty").value);
    propertyFD.append("bhkType", this.propertyForm.get("bhkType").value);
    propertyFD.append("propertySize", this.propertyForm.get("propertySize").value);
    propertyFD.append("facing", this.propertyForm.get("facing").value);

    propertyFD.append("locationDetails", JSON.stringify(lD));

    propertyFD.append("rent", this.propertyForm.get("rent").value);
    propertyFD.append("deposit", this.propertyForm.get("deposit").value);
    propertyFD.append("furnisedType", this.propertyForm.get("furnisedType").value);
    propertyFD.append("tenants", this.propertyForm.get("tenants").value);

    for (let i = 0; i < imagesLength; i++)
      propertyFD.append("images", images[i], images[i].name);


    propertyFD.append("s3BucketImg", JSON.stringify(s3BucketImg));
    propertyFD.append("propertyInfo", JSON.stringify(pD));
    propertyFD.append("isPublished", this.propertyForm.get("isPublished").value);


    this.callingApi = true;
    if (!_id)
      this.save(propertyFD, indexNumber);
    else
      this.update(propertyFD, indexNumber);
  }

  save(propertyFD, indexNumber) {
    this.propertyService.addProperty(propertyFD).subscribe((res) => {
      this.propertyForm.patchValue({ _id: res.data._id })
      this.completedStage.push(indexNumber);
      this.activeProperty += 1;
      this.callingApi = false;
    });
  }

  update(propertyFD, indexNumber) {
    propertyFD.append("_id", this.propertyForm.get("_id").value);
    this.propertyService.updateProperty(propertyFD).subscribe((res) => {
      if (indexNumber != -1) {
        this.completedStage.push(indexNumber);
        this.activeProperty += 1;
        this.callingApi = false;

        if (res && res.s3BucketImg && res.s3BucketImg.length > 0) {
          this.s3BucketImg = res.s3BucketImg;
          this.propertyForm.patchValue({
            's3BucketImg': this.s3BucketImg,
            'images': ''
          });
        }

      }
      else {
        this.router.navigate(['/dashboard'], { relativeTo: this.activatedRoute });
      }
    });
  }

  changePropertyDetails(currentIndex, previousIndex) {
    if (this.completedStage && this.completedStage.includes(previousIndex)) {
      this.activeProperty = currentIndex;
    } else {
      this.activeShake = this.activeProperty + '';
      this.commonService.showToast('Save your details!',0);
      setTimeout(() => { this.activeShake = ""; }, 500);
    }
  }

  goBack() {
    this.activeProperty -= 1;
  }

  searchLocality() {
    let query = this.propertyForm.get('locationDetails').get('locality').value;
    if (query && query.length > 3) {
      this.propertyService.getLocalityGoogle(query).subscribe((res) => {
        this.localitySet = res.predictions;
      })
    }
  }

  getPlaceDetail(placeId: string) {
    if (placeId) {
      this.propertyService.getPlaceDetail(placeId).subscribe((res) => {
        console.log(res);
        this.localitySelected = res.result;
        this.propertyForm.patchValue({
          locationDetails: {
            locality: res.result.formatted_address
          }
        });
        this.localitySet = [];
      })
    }
  }

  publishProperty(e: any) {
    e.preventDefault();
    this.propertyForm.patchValue({ 'isPublished': true });
    this.savePropertyDetails(-1);
  }
}
