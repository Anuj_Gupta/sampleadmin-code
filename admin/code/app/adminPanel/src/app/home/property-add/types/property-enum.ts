const roomType = [
    { id: 1, key: "PG-Boys" },
    { id: 2, key: "PG-Girls" },
    { id: 3, key: "Rent" },
    { id: 4, key: "PG-Girls/Boys" }
];

const bedLocation = [
    { id: 1, key: "BEDROOM" },
    { id: 2, key: "BEDROOM/HALL" },
    { id: 3, key: "FULL APARTMENT" },
    { id: 4, key: "HALL" }
];


const availabilty = [
    { id: 1, key: "ALL" },
    { id: 2, key: "GIRLS" },
    { id: 3, key: "BOYS" },
    { id: 4, key: "FAMILY" },
    { id: 5, key: "CORPORATE" }
];


const bhkType = [
    { id: 1, key: "1 RK" },
    { id: 2, key: "1 BHK" },
    { id: 3, key: "2 BHK" },
    { id: 4, key: "3 BHK" },
    { id: 5, key: "4+ BHK" }
]

const facing = [
    { id: 1, key: "North" },
    { id: 2, key: "South" },
    { id: 3, key: "East" },
    { id: 4, key: "West" },
    { id: 5, key: "North-East" },
    { id: 6, key: "South-East" },
    { id: 7, key: "North-West" },
    { id: 8, key: "South-West" },
];

const city = [
    { id: "Mumbai", key: "Mumbai" },
    { id: "Kolkata", key: "Kolkata" },
    { id: "NCR", key: "NCR" },
    { id: "Bangalore", key: "Bangalore" },
    { id: "Pune", key: "Pune" },
    { id: "Hyderabad", key: "Hyderabad" },
    { id: "Chennai", key: "Chennai" },
    { id: "Surat", key: "Surat" },
    { id: "Visakhapatnam", key: "Visakhapatnam" },
    { id: "Kanpur", key: "Kanpur" },
];

const furnisedType = [
    { id: 1, key: "Fully Furnished" },
    { id: 2, key: "Semi-Furnished" },
    { id: 3, key: "UnFurnished" },
];

const tenants = [
    { id: 1, key: "Doesn't Matter" },
    { id: 2, key: "Family" },
    { id: 3, key: "Bachelors" },
    { id: 4, key: "Company" },
];

const filterName = {
    roomType:'Room Type',
    bhkType:'BHK',
    bedLocation:'Bed Location',
    availabilty:'Availabilty',
    noOfAvailabilty:'No Of availabilty',
    range:'Range',
    isPublished:'Published',
    facing:'Property Facing',
    city:'City',
    propertyTitle:'Property Title'
}



const PropertyEnum = {
    roomType,
    bedLocation,
    availabilty,
    bhkType,
    facing,
    city,
    furnisedType,
    tenants,
    filterName
};

export { PropertyEnum };