import { Component, OnInit } from '@angular/core';
import { PropertyService, CommonService } from '../../shared/shared.module';
import { Router, ActivatedRoute } from "@angular/router";
import { PropertyEnum } from '../property-add/types/property-enum';
import { FormBuilder, FormGroup } from '@angular/forms';
import propertyTypes from './types/property-types';

@Component({
  selector: 'app-property-list',
  templateUrl: './property-list.component.html',
  styleUrls: ['./property-list.component.scss']
})
export class PropertyListComponent implements OnInit {
  propertys: any[];
  pagination: any = {
    page: 1,
    limit: 20
  };
  filterForm: propertyTypes;
  propertyEnum = PropertyEnum;
  propertyList = {
    count: 0,
    propertyList: [],
    total: {
      relation: "",
      value: 0
    }
  }
  expanded = false;

  constructor(private propertyService: PropertyService, private commonService: CommonService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getPropertyList('');
    this.initiFilter();
  }

  getPropertyList(query: string) {
    let q = `?page=${this.pagination.page}&limit=${this.pagination.limit}&${query}`;
    this.propertyService.getPropertyList(q)
      .subscribe((res) => {
        this.propertyList = res.data;
        // this.propertys = res.data.propertyList;
      })
  }

  initiFilter() {
    this.filterForm = {
      roomType: [],
      availabilty: [],
      bedLocation: [],
      noOfAvailabilty: [],
      range: '',
      bhkType: [],
      isPublished: true,
      facing: [],
      city: [],
      propertyTitle: '',
    }
  }



  editProperty(propertyId: string) {
    this.router.navigate([`/dashboard/edit-property/${propertyId}`], { relativeTo: this.activatedRoute });
  }



  formChangeEvent(evt: any) {
    let { checked } = evt;
    let { name, value } = evt.element;
    if (checked) {
      this.filterForm[name].push(value);
    } else {
      this.filterForm[name] = this.filterForm[name].filter(n => n != value);
    }
    this.getList();
  }

  bySearch(evt: any) {
    let { name, value } = evt.target;
    this.filterForm[name] = value;
    this.getList();
  }


  getList() {
    let query = "";
    Object.keys(this.filterForm).map((key) => {
      if (this.filterForm[key]) {
        if (typeof this.filterForm[key] == 'object' && this.filterForm[key].length > 0) {
          query += `${key}=${this.filterForm[key].join(',')}&`;
        }
        if (typeof this.filterForm[key] == 'string') {
          query += `${key}=${this.filterForm[key]}&`;
        }
      }

    });
    this.getPropertyList(query);
  }

  removeFilter(keyName) {
    this.filterForm[keyName] = [];
    this.getList();

    let allCheckBox = document.getElementsByName(keyName);
    Object.keys(allCheckBox).map((c) => {
      allCheckBox[c].checked = false;
    })
  }

  getChip(chipData) {
    console.log(chipData);
  }


  showCheckboxes(filterType: string) {
    let currentCheck = document.getElementById(filterType);
    if (!this.expanded) {
      currentCheck.style.display = "block";
      this.expanded = true;
    } else {
      currentCheck.style.display = "none";
      this.expanded = false;
    }

    //forcefully close other select boxes;
    let allCheckBoxes = document.getElementsByClassName('checkboxes');
    Object.keys(allCheckBoxes).map((c) => {

      if (allCheckBoxes[c].id != currentCheck.id) {
        allCheckBoxes[c].style.display = allCheckBoxes[c].style && 'none';
      }

    })
  }

  getPropertyName(propertyNum: number, propertyType: string) {
    let name = this.propertyEnum[propertyType].filter(pt => pt.id == propertyNum);
    return name.length > 0 ? name[0].key : '';
  }

  paginationEvent(isPlus: boolean) {
    if (isPlus) {
      ++this.pagination.page;
      this.getPropertyList('');
    } else if (this.pagination.page != 1) {
      --this.pagination.page;
      this.getPropertyList('');
    }
  }

  changeStatus(propertyId: string, isPublished: boolean) {
    if (propertyId) {
      let body = {
        published: [],
        unpublished: []
      };

      if(isPublished){
        body.published.push(propertyId);
      }else{
        body.unpublished.push(propertyId);
      }
      this.propertyService.changeStatus(body)
      .subscribe((res)=>{
        // this.commonService.
        this.getList();
      })
    }
  }

}
