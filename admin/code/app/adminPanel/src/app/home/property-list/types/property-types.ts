
 interface propertyTypes {
    roomType:string[],
    bhkType:string[],
    bedLocation:string[],
    availabilty:string[],
    noOfAvailabilty:string[],
    range:string,
    isPublished:boolean,
    facing:string[],
    city:[],
    propertyTitle:string,
 }


 export default propertyTypes;