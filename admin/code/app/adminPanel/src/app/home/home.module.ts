import { NgModule,NO_ERRORS_SCHEMA } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { PropertyListComponent } from './property-list/property-list.component';
import { PropertyAddComponent } from './property-add/property-add.component';


//INFO:Relelated to property
const  PROPTERTY = [PropertyListComponent,PropertyAddComponent];



@NgModule({
  declarations: [...PROPTERTY],
  imports: [
    HomeRoutingModule,
    CommonModule,
    RouterModule,
    SharedModule,
  ],
  schemas: [NO_ERRORS_SCHEMA]

})
export class HomeModule { }
