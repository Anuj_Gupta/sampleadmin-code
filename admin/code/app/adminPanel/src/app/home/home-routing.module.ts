import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard, NoAuthGuard } from '../shared/shared.module';
import { MainLayoutComponent } from '../shared/shared.module';

//Componet
import { PropertyListComponent } from './property-list/property-list.component';
import { PropertyAddComponent } from './property-add/property-add.component';


const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuard],
        component: MainLayoutComponent,
        children: [
            { path: '', component: PropertyListComponent },
            { path: 'add-property', component: PropertyAddComponent },
            { path: 'edit-property/:propertyId', component: PropertyAddComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
