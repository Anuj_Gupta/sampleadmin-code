const mongoose = require('mongoose'),
    Schema = mongoose.Schema;
/**
 * AccessToken Schema
 */

let accessToken = new Schema({
    userId:{
      type:Schema.Types.ObjectId,
      required:[true,'require userId']  
    },
    ttl:{
        type:Number,
        default:
        //one week token
        //mili*sec*hour*hour in day*no of day
        1000 * 60 * 60 * 24 * 7
    },
    createdAt:{
        type:Date,
        default:Date.now
    }
})

module.exports = mongoose.model('accessToken',accessToken);