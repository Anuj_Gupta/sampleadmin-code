const connectToDatabase = require('../db/db');
const jwt = require('jsonwebtoken')
const accessTokenObj = require('../model/accessToken');
const env = require('../config');

// let validateToken = async (token) => {
//     let userInfo = {};
//     return connectToDatabase()
//         .then(() => {
//             return verifyToken(token)
//         })
//         .then((info) => {
//             userInfo = info;
//             return accessTokenObj.findById(userInfo.accessId).lean().exec()
//         })
//         .then((info) => {
//             if (info) {
//                 return validateExpiry(info)
//             }
//         })
//         .then((isValid) => {
//             if (isValid)
//                 return Promise.resolve(userInfo);
//             else
//                 return Promise.reject("Expired token");
//         })
//         .catch((err) => {
//             console.log(err);
//             return Promise.reject('Something went wrong');
//         })
// }


const verifyToken = (token) => {
    let user = {};
    return Promise.resolve(1)
        .then(() => {
            token = token.split(/\s+/).pop();
            user = jwt.verify(token, env.JWT);
            return Promise.resolve(user);
        })
        .catch((err) => {
            return Promise.reject('Expired Token');
        })

}

// let validateExpiry = (tokenDetail) => {
//     return Promise.resolve(1)
//         .then(() => {
//             let currentTime = new Date().getTime();
//             let tokenCreated = new Date(tokenDetail.createdAt).getTime();
//             let ttl = new Date(tokenCreated + tokenDetail.ttl).getTime();
//             let isValid = currentTime > tokenCreated && currentTime < ttl;
//             console.log('isValid', isValid);
//             return Promise.resolve(isValid);
//         })
//         .catch((err) => {
//             console.log(err);
//             return Promise.reject("Expired token");
//         })
// }


module.exports = verifyToken;