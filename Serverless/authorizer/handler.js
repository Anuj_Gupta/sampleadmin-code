
const watchmen = require('./service/watchMen');


module.exports.verifyJWT = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  let token = event.authorizationToken ? event.authorizationToken : "";
  if (token) {
    return watchmen(token)
      .then((re) => {
        context.succeed(generatePolicy('user', 'Allow', event.methodArn, re))
      })
      .catch((err) => {
        console.log(err);
        context.fail("Unauthorized");
      })
  }
  else {
    context.fail("Unauthorized");
  }
};

var generatePolicy = function (principalId, effect, resource, userDetial) {
  console.log("resource", resource);
  var authResponse = {};
  authResponse.principalId = principalId;
  if (effect && resource) {
    var policyDocument = {};
    policyDocument.Version = '2012-10-17'; // default version
    policyDocument.Statement = [];
    var statementOne = {
      Action: 'execute-api:Invoke', // default action
      Effect: effect,
      Resource: resource,
    };
    policyDocument.Statement.push(statementOne);
    authResponse.policyDocument = policyDocument;
  }
  // Optional output with custom properties of the String, Number or Boolean type.
  //$event.requestContext.authorizer.key retrive from this.
  authResponse.context = {
    userDetial: JSON.stringify(userDetial)
  };
  return authResponse;
}
