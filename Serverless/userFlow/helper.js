const config = require('./config');

let sendAWSSMS = (message, mobileNo) => {
    if (message && mobileNo) {
        let AWS = require('aws-sdk');
        AWS.config.update({ region: config.region, accessKeyId: config.accessKeyId, secretAccessKey: config.secretAccessKey });
        mobileNo = mobileNo.length == 10 ? "+91" + mobileNo : mobileNo;
        // Create publish parameters
        let params = {
            Message: message,
            PhoneNumber: mobileNo
        };

        // Create promise and SNS service object
        let publishTextPromise = new AWS.SNS({
            apiVersion: '2010-03-31'
        }).publish(params).promise();

        // Handle promise's fulfilled/rejected states
        return publishTextPromise
            .then((data) => {
                console.log("MessageID is " + data.MessageId);
                return Promise.resolve(true);
            })
            .catch((err) => {
                console.error(err, err.stack);
                return Promise.reject(false);
            });
    }
}

let msg91 = (message, mobileNo, isTransaction) => {
    // const request = require('request');
    const http = require('http');
    let msg91Url = config.msg91;
    msg91Url += `route=${isTransaction ? 4 : 1}&mobiles=${mobileNo}&message=${message}`;
    console.log({ msg91Url });
    return new Promise((res, rej) => {
        http.get(msg91Url, (resp) => {
            let data = '';

            // A chunk of data has been recieved.
            resp.on('data', (chunk) => {
                data += chunk;
            });

            // The whole response has been received. Print out the result.
            resp.on('end', () => {
                data = JSON.parse(data);
                if (data.type == "success")
                    res(true);
            });

        }).on("error", (err) => {
            console.log("Error: " + err.message);
            rej(err);
        });
    });
}


let generateOTP = () => {
    let otp = '';
    for (let i = 0; i < 6; i++)
        otp += Math.floor(Math.random() * 10);
    return otp;
}


const helper = {
    sendAWSSMS,
    generateOTP,
    msg91
}

module.exports = helper;