const bcrypt = require('bcryptjs');
const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

const SALT_WORK_FACTOR = 10;

let userInfoSchema = new Schema({
    mobile: {
        type: String,
        required: [true, 'require mobile'],
        unique: true,
        trim: true,
        validate: {
            validator: function (v) {
                return /^[6-9]{1}\d*$/.test(v);
            },
            message: props => `${props.value} is not a valid mobile!`
        },
    },
    email: {
        type: String,
        // required: [true, 'require emailId'],
        // unique: true,
        default:"",
        trim: true,
        validate: {
            validator: function (v) {
                return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v);
            },
            message: props => `${props.value} is not a valid email!`
        },
    },
    otp: {
        type: String,
        default: null,
        trim: true,
        select: false
    },
    password: {
        type: String,
        default: null
    },
    loginCount: {
        type: Number,
        default: 0
    },
    isMobileVerified: { type: Boolean, default: false },
    isBroker: { type: Boolean, default: false },
    referredBy: {
        type: Schema.Types.ObjectId,
        default: null,
        trim: true
    },
    created: {
        type: Date,
        default: Date.now
    },
    modified: {
        type: Date,
        default: null
    },

});

userInfoSchema.pre('findOneAndUpdate', function (next) {
    let user = this;
    // only hash the password if it has been modified (or is new)
    if (!user._update['password']) return next();
    generatePassHash(user._update['password'])
        .then((hashK) => {
            user._update['password'] = hashK;
            user._update['modified'] = new Date();
            next();
        })
        .catch((err) => {
            return Promise.reject(err);
        })
});


userInfoSchema.statics.validatePass = function (plainPass, hashV) {
    return bcrypt.compare(plainPass, hashV)
        .then((res) => {
            let a = res ? Promise.resolve(true) : Promise.reject("Invalid email or password");
            return a;
        })
}




const generatePassHash = (pass) => {
    if (pass) {
        return bcrypt.genSalt(SALT_WORK_FACTOR)
            .then((salt) => {
                return bcrypt.hash(pass, salt)
            })
            .then((hash) => {
                return Promise.resolve(hash);
            })
            .catch((err) => {
                console.log(err);
                return Promise.reject("error while generating hash");
            })
    }
    else {
        return Promise.reject("Blank password");
    }
}

userInfoSchema.index({ mobile: 1, mobile: -1 }, { name: 'index on mobile' });
module.exports = mongoose.model('usersInfo', userInfoSchema, 'usersInfo');
