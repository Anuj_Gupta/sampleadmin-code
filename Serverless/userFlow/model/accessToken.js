const mongoose = require('mongoose'),
    Schema = mongoose.Schema;
const jwt = require('jsonwebtoken')
const env = require('../config');

/**
 * AccessToken Schema
 */

let accessToken = new Schema({
    userId:{
      type:Schema.Types.ObjectId,
      required:[true,'require userId']  
    },
    ttl:{
        type:Number,
        default:
        //one week token
        //mili*sec*hour*hour in day*no of day
        1000 * 60 * 60 * 24 * 7
    },
    createdAt:{
        type:Date,
        default:Date.now
    }
})

accessToken.statics.getToken = function(userD){
    return Promise.resolve(1)
    .then(()=>{
        return jwt.sign(userD,env.JWT,{ expiresIn: '7d' });
    })
    .catch(err=>{
        console.log(err);
        return Promise.reject('Something went wrong');
    })
}


module.exports = mongoose.model('accessToken',accessToken);