const connectToDatabase = require('../db/db');
let userModel = require('../model/userInfo');
let accessTokenM = require('../model/accessToken');
let helper = require('../helper');
let config = require('../config');



const otpGeneration = (userBody) => {
	return connectToDatabase()
		.then(() => {
			userBody = JSON.parse(userBody)
			return userBody
		})
		.then(() => {
			return !userBody.mobile ? Promise.reject('Invalid request body') : true;
		})
		.then(() => {
			if (!userBody.password) {
				return otpGenerationProcess(userBody)
			}
			else {
				return passwordVerification(userBody)
			}
		})
		.catch(err => {
			console.log(err);
			let e = typeOfError(err);
			return Promise.reject(e);
		})
}


const verifyOTP = (userBody) => {
	let userInfo;
	let response = {
		status: 0,
		msg: '',
		data: ''
	};
	return connectToDatabase()
		.then(() => {
			userBody = JSON.parse(userBody)
			return userBody
		})
		.then(() => {
			return !userBody.mobile || !userBody.otp ? Promise.reject('Invalid request body') : true;
		})
		.then(() => {
			let query = { 'mobile': userBody.mobile, 'otp': userBody.otp };
			let condition = { '$inc': { loginCount: 1 }, '$set': { isMobileVerified: true } }
			return userModel.findOneAndUpdate(query, condition, { new: true }).lean().exec()
		})
		.then((userD) => {

			if (!userD)
				return Promise.reject('Invalid OTP');

			userInfo = userD;
			let aT = new accessTokenM();
			aT.userId = userD._id;
			return aT.save()
		})
		.then((aId) => {
			if (aId) {
				userInfo["accessId"] = aId._id;
				delete userInfo.password;
				// delete userInfo.otp;
				return accessTokenM.getToken(userInfo)
			} else {
				return Promise.reject('Failed while generating token');
			}
		})
		.then((token) => {
			response = {
				status: 1,
				msg: "login success",
				data: { AES: token, loginCount: userInfo.loginCount }
			};
			return Promise.resolve(response);
		})
		.catch((err) => {
			console.log(err);
			response.msg = err.constructor == String ? err : "Something went wrong!";
			return Promise.reject(response);
		})
}

const savePassword = (userBody) => {
	let response = {
		status: 0,
		msg: '',
		data: ''
	};
	return connectToDatabase()
		.then(() => {
			userBody = JSON.parse(userBody)
			return userBody
		})
		.then(() => {
			return !userBody.mobile || !userBody.password ? Promise.reject('Invalid request body') : true;
		})
		.then(() => {
			let query = { 'mobile': userBody.mobile };
			let condition = { 'password': userBody.password }
			return userModel.findOneAndUpdate(query, condition, { new: true }).lean().exec()
		})
		.then((userD) => {
			if (userD) {
				response = {
					status: 1,
					msg: "Password Saved successfully",
					data: ''
				};
				return Promise.resolve(response);
			}
			else {
				return Promise.reject('Failed while saving password');
			}
		})
		.catch((err) => {
			console.log(err);
			let e = typeOfError(err);
			return Promise.reject(e);
		})
}

const otpGenerationProcess = (userBody) => {
	let response = {
		status: 0,
		msg: '',
		data: ''
	};
	return Promise.resolve()
		.then(() => {
			let o = helper.generateOTP();
			let query = { 'mobile': userBody.mobile };


			//Generate new object of modal so that when it's insert push whole object using setOnInsert
			let userObj = new userModel(userBody);
			let tempUserObj = userObj.toObject();
			//Delete both keys since '$set' operator will take care of this
			delete tempUserObj.otp;
			delete tempUserObj.modified;
			delete tempUserObj.password;

			userObj = tempUserObj

			let condition = { '$set': { 'otp': o }, '$setOnInsert': userObj };
			return userModel.findOneAndUpdate(query, condition, { upsert: true, new: true }).lean().exec()
		})
		.then((uI) => {
			if (uI) {
				let msg = config.forgetPassword;
				msg = msg.replace("$otp:", uI.otp);
				return helper.msg91(msg, uI.mobile, true)
			} else {
				return Promise.reject('Something went wrong!');
			}
		})
		.then((isSend) => {
			if (isSend) {
				response = {
					status: 1,
					msg: 'OTP send successfully',
					data: ''
				}
				return Promise.resolve(response);
			} else {
				return Promise.reject('Failed while sending otp')
			}
		})
}


const passwordVerification = (userBody) => {
	let userInfo;
	let response = {
		status: 0,
		msg: '',
		data: ''
	};

	return Promise.resolve()
		.then(() => {
			let query = { 'mobile': userBody.mobile };
			return userModel.findOne(query).lean().exec()
		})
		.then((userD) => {
			if (userD) {
				userInfo = userD;
				return userModel.validatePass(userBody.password, userD.password)
			} else {
				return Promise.reject('No user found');
			}
		})
		.then(() => {
			let aT = new accessTokenM({
				userId: userInfo._id
			});
			return aT.save()
		})
		.then((aId) => {
			if (aId) {
				userInfo["accessId"] = aId._id;
				delete userInfo.password;

				return accessTokenM.getToken(userInfo)
			} else {
				return Promise.reject('Failed while generating token');
			}
		})
		.then((token) => {
			response = {
				status: 1,
				msg: "login success",
				data: { AES: token, loginCount: userInfo.loginCount }
			};
			return Promise.resolve(response);
		})
}

const typeOfError = (err) => {
	let response = {
		status: 0,
		msg: ""
	};
	if (err) {
		if (err.name && err.name == 'ValidationError') {
			for (field in err.errors) {
				response.msg += err.errors[field].message + "_";
			}
			return response;
		} else {
			response.msg = err.constructor == String ? err : "Something went wrong!";
			return response;
		}
	} else {
		response.msg = 'Something went wrong!';
		return response;
	}
}



const userAction = {
	verifyOTP,
	otpGeneration,
	savePassword
}


module.exports = userAction