'use strict';
const userOperation = require('./service/userProfile');

let requestOtp = async (event, context) => {

  context.callbackWaitsForEmptyEventLoop = false;
  // console.log({event});
  return userOperation.otpGeneration(event.body)
    .then((re) => {
      console.log(re);
      return {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(re)
      }
    })
    .catch((err) => {
      console.log(err);
      return {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(err)
      }
    })
};

let verifyotp = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return userOperation.verifyOTP(event.body)
    .then((re) => {
      console.log(re);
      return {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(re)
      }
    })
    .catch((err) => {
      console.log(err);
      return {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(err)
      }
    })
};


let savePassword = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return userOperation.savePassword(event.body)
    .then((re) => {
      console.log(re);
      return {
        statusCode: 200,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(re)
      }
    })
    .catch((err) => {
      console.log(err);
      return {
        statusCode: 500,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify(err)
      }
    })
};





const handler = {
  verifyotp,
  requestOtp,
  savePassword
};

module.exports = handler;