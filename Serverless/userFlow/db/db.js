const env = require('../config');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
let isConnected;

module.exports = connectToDatabase = () => {
  console.log('Inside connectToDatabase',env.DB);
  if (isConnected) {
    console.log('=> using existing database connection');
    return Promise.resolve();
  }

  console.log('=> using new database connection');
  return mongoose.connect(env.DB)
    .then(db => { 
      isConnected = db.connections[0].readyState;
      return Promise.resolve();
    });
};